import json
import zmq
import logging
import time
import threading

from datetime import datetime

log = logging.getLogger('DataSender')


class DataSenderProtocol(object):

    def __init__(self, address, time, source_unique_id):
        self.time = time
        self.buffer = []
        self.source_id = source_unique_id
        self._init_connection(address)
        sender_thread = threading.Thread(target=self._send_buffer_content)
        sender_thread.start()

        log.info('Sender Protocol initiated for source number {0} with {1}'
                 ' seconds between messages send to server.'
                 .format(self.source_id, self.time))

    def _init_connection(self, address):
        try:
            address = "tcp://" + address
            context = zmq.Context()
            log.info("Connecting to server...")
            self.socket = context.socket(zmq.REQ)
            self.socket.connect(address)
        except Exception as e:
            log.error("Failed to connect to the server: {0}".format(e))

    def _get_date_time(self):
        return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def _send_buffer(self):
        try:
            _message = {'source_id': self.source_id, 'buffer': self.buffer}
            _message = json.dumps(_message)
            log.info('Sending currently buffer to the server, with {0} items.'
                     .format(len(self.buffer)))
            self.socket.send_json(_message)
        except Exception as e:
            log.error("Something went wrong with the request {0}".format(e))

    def _insert_timestamp(self, value):
        _timestamp = self._get_date_time()
        value['timestamp'] = _timestamp
        return value

    def store_value(self, value):
        _datagram = self._insert_timestamp(value)
        self.buffer.append(_datagram)

    def _send_buffer_content(self):
        while True:
            time.sleep(self.time)
            self._send_buffer()
            feedback = json.loads(self.socket.recv_json())
            if feedback:
                self._handle_feedback(feedback)
            else:
                log.error("Communication problem: wrong or empty response "
                          "from server")

    def _handle_feedback(self, feedback):
        if feedback['status'] == 'ok':
            log.info("Value successful stored in the server, cleaning local"
                     " buffer.")
            self.buffer = []
        else:
            log.error("Something went wrong with the server during the "
                      "request. Sending it again...")
            self._send_buffer_content()
