import random
from argparse import ArgumentParser
import logging
import time
import sys
from sender_protocol import DataSenderProtocol

log = logging.getLogger('DataSender')


class DataSource(object):

    def __init__(self):
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
        self._arg_manager()
        self._init_data_source()

    def _init_data_source(self):
        self.data_sender = DataSenderProtocol(self.address,
                                              self.time,
                                              self._uid)
        self._main_loop()
        log.info("The Data Capture was started.")

    def _arg_manager(self):
        arg_manager = ArgumentParser()
        arg_manager.add_argument('-a', '--address',
                                 action='store',
                                 default='localhost:5555',
                                 type=str,
                                 help='Set the host and port.')

        arg_manager.add_argument('-w', '--wait_time',
                                 action='store',
                                 default=10,
                                 type=int,
                                 help='Set the time to send the buffer to'
                                      ' the server.')

        arg_manager.add_argument('-i', '--id',
                                 action='store',
                                 type=str,
                                 help='Set the source unique ID.')

        args = arg_manager.parse_args()

        try:
            if args.address is not None:
                self.address = args.address

            if args.wait_time is not None:
                self.time = args.wait_time

            if args.id is not None:
                self._uid = args.id

        except Exception as e:
            log.error(e)

    def _main_loop(self):
        while True:
            _time_to_new_collect = random.randint(1, 4)
            log.info('Getting new value in %s seconds...' %
                     (_time_to_new_collect))
            time.sleep(_time_to_new_collect)
            value = random.random()
            log.info('Get new value %s' % (value))
            value_type = 'random'
            self.data_sender.store_value({'value': value, 'type': value_type})


if __name__ == '__main__':
    DataSource()
