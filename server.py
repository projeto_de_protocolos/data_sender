import json
import zmq
import threading
import logging
import sys

log = logging.getLogger('SimpleStorageServer')


class BasicStorageServer(object):

    def __init__(self):
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
        context = zmq.Context()
        self.socket = context.socket(zmq.REP)
        self.socket.bind("tcp://*:5555")
        self.buffer = []
        thread_server = threading.Thread(target=self._main_loop())
        thread_server.start()

    def _main_loop(self):
        while True:
            #  Wait for next request from client
            log.info('Waiting items to store...')
            try:
                message = json.loads(self.socket.recv_json())
                new_buffer = message['buffer']
                for item in new_buffer:
                    log.info(item)
                    self.buffer.append(item)
                log.info('The buffer have %s items.' % (len(self.buffer)))
                feedback = {'status': 'ok', 'reason': ''}
                self.socket.send_json(json.dumps(feedback))
            except Exception as error:
                feedback = {'status': 'failure', 'reason': error}
                self.socket.send_json(json.dumps(feedback))


if __name__ == '__main__':
    BasicStorageServer()
