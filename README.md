# DataSender

Simple protocol proposed by Marcelo Sobral in the Protocol Project's discipline. The idea is to implement a transport protocol like routine to store data from a generic source in a server.

The protocol runs over TCP connection and use Python in the source code. It must receive data at any time from the source, store it in a buffer and send all the buffered data to the server by a pre-configured time.

The protocol must receive the data source identifier, the time to send information to the server  and the server address in the start of the program. After this, the data source can use the store_data() method to store information in the protocol,
passing the value and the information about the value. The protocol will store it in a JSON syntax with the timestamp and the origin ID, and after the preconfigured time, all the buffer will be sent to the server. Then, the protocol will wait for the response of the server. If the server sends status: 'ok'.

Data package format added to the buffer:
``` python
{'value': VALUE, 'type': VALUE_TYPE, 'timestamp': TIMESTAMP}
```

Message (datagram send to server) format:
```python
{'source_id': ID, 'buffer': BUFFER_LIST}
```

## Test usage:

In this repository, there's two files to help testing the Protocol. One is the source.py, which implements a Data Acquisition Board. The second one is the server.py, which implements a simple storage server.

### Source emulator

The source.py can receive three arguments: server address (-a, --address), sending time (-w, --wait_time) and source identification (-i, --id). The source.py must receive the ID parameter in order to work. The address and wait_time are opitional, and the program will use the default values if none is passed.

Example usage of source.py:
```python
python source.py -a localhost:5555 -w 10 -i 01
```

### Server emulator

The server.py have hard-coded host and port (localhost:5555), so it is nice to set your DataSource program to send the information to that address. The server implementation is realy simple, intended just to test the protocol workflow. It only receive the data, try to store it in a local list and tell the Protocol if it was successful or not.

## Implementing your own data source using the SenderProtocol:

In order to use the SenderProtocol, all you have to do is create an instance of it's class inside your program, passing the server address, the time for the buffer update and an unique ID. The server must be prepare to send responses to the protocol in the correct format shown bellow.

```python
server_response = {'status': ['ok', 'failure'], 'reason': ['', REASON]}
```
